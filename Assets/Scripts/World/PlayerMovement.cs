﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	public int speed;

	TimeCheck timeCheck = new TimeCheck();

	Direction dir = Direction.None;

	bool actedThisFrame = false;

	void Update()
	{
		actedThisFrame = false;
	}

	void FixedUpdate () 
	{
		GetKeys ();

		if(!actedThisFrame){
			
		if (timeCheck.UpdateTime (speed)) {

				if (dir != Direction.None) {
				
					MovePlayer (dir);

					dir = Direction.None;

					actedThisFrame = true;
				}
			}
		}
	}
	void GetKeys()
	{
		if(hardInput.GetKeyDown("Left"))
			dir = Direction.Left;
		if(hardInput.GetKeyDown("Right"))
			dir = Direction.Right;
		if(hardInput.GetKeyDown("Up"))
			dir = Direction.Up;
		if(hardInput.GetKeyDown("Down"))
			dir = Direction.Down;

		if (dir == Direction.None) {
			
			if(hardInput.GetKey("Left"))
				dir = Direction.Left;
			if(hardInput.GetKey("Right"))
				dir = Direction.Right;
			if(hardInput.GetKey("Up"))
				dir = Direction.Up;
			if(hardInput.GetKey("Down"))
				dir = Direction.Down;
		}
	}
	void MovePlayer(Direction dir)
	{
		Debug.Log (dir);
		Vector3 pos = DirectionManager.Dir2Vect(dir);

		Debug.Log (pos);

		transform.position += pos;
	}
}
