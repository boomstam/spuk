﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitAmmo : MonoBehaviour 
{
	public GameObject[] ammoPrefabs;
	public GameObject[] trailPrefabs;

	public int stepCount;

	public int ammoCount;

	public void CreateAmmo(int type, int trailType, Vector3 pos, Vector2[] waveCoor, float deltaOffset)// -1 trailType = none
	{
		GameObject ammo = GameObject.Instantiate (ammoPrefabs[type]);

		float ammoSize = ammo.transform.localScale.y;
		Vector3 posOffset = new Vector3 (ammoSize, -ammoSize, 0);
		ammo.transform.position = pos + posOffset;

		ammo.transform.SetParent (gameObject.transform);

		ammo.name = "ammo_" + ammoCount.ToString();

		ammo.AddComponent<AmmoMovement> ().waveCoor = waveCoor;

		ammo.GetComponent<AmmoMovement> ().deltaOffset = deltaOffset;

		AddTrail (trailType, ammo);

		ammoCount++;
	}
	void AddTrail(int trailType, GameObject ammo)
	{

	}
	public Vector2[] GetWaveCoor(float amp, float freq, float phase, float speed)// 1.0 = default speed
	{
		Vector2[] waveCoor = new Vector2[stepCount];

		float previousX = 0;

		for(int step = 0; step < stepCount; step++){
		
			float x = step * 2 * Mathf.PI / stepCount;

			float deltaX = x - previousX;

			float y = amp * Mathf.Sin(freq * x + phase);

			waveCoor [step] = new Vector2 (deltaX * speed, y);

			previousX = x;
		}
		return waveCoor;
	}
}
