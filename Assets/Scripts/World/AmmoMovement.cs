﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoMovement : MonoBehaviour 
{
	public Vector2[] waveCoor;// THESE TWO VARIABLES MUST BE INITIALISED WHEN ADDED TO OBJECT
	public float deltaOffset;

	int currentIndex = 0;
	float offset = 1;

	void FixedUpdate()
	{
		Vector2 vect = waveCoor [currentIndex];

		gameObject.transform.position += new Vector3(vect.x, vect.y * offset, 0);

		if (currentIndex >= waveCoor.Length - 1) {

			currentIndex = 0;
		}
		offset += deltaOffset;

		currentIndex++;
	}
}
