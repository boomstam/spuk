﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

	public float maxSpeed = 5f;

	// Update is called once per frame
	void Update () {
		// Returns a FLOAT from -1.0 to +1.0
		Vector3 pos = transform.position;
		// avoid if-statement (if GetAxis("Vertical") == ...) by multiplying
		Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);
		// feed vector to real position variable
		pos += transform.rotation * velocity;

		transform.position = pos;
	}
}
