﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour 
{
	InitAmmo initAmmo = new InitAmmo();// ONLY POSSIBLE IF INITIALISED IN Start()

	void Start()
	{
		initAmmo = transform.parent.gameObject.GetComponentInChildren<InitAmmo>();
	}
	void Update()
	{
		if (Input.GetButton ("Fire1")) {

			Debug.Log("fireç");

			int type = 0;

			Vector2[] waveCoor = initAmmo.GetWaveCoor (1, 1, 0, 0.2f);

			float deltaOffset = 0f;// Increasing amp over time

			initAmmo.CreateAmmo (0, 0, gameObject.transform.position, waveCoor, deltaOffset);

			foreach(Vector2 vect in waveCoor){

				Debug.Log (vect);
			}
		}
	}
}
