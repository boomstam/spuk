﻿using UnityEngine;
using System.Collections;

public static class CanvasHandler
{
	public static void HideCanvas(GameObject canvas)
	{
		Debug.Log ("Hide " + canvas.name);
		canvas.GetComponent<CanvasGroup> ().alpha = 0;
		canvas.GetComponent<CanvasGroup> ().interactable = false;
		canvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}
	public static void ShowCanvas(GameObject canvas)
	{
		Debug.Log ("Show " + canvas.name);
		canvas.GetComponent<CanvasGroup> ().alpha = 1;
		canvas.GetComponent<CanvasGroup> ().interactable = true;
		canvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
	}
}
