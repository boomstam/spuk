﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Block
{
	UpQuark,
	DownQuark,
	CharmQuark,
	StrangeQuark,
	TopQuark,
	BottomQuark,

	Electron,
	ElectronNeutrino,
	Muon,
	MuonNeutrino,
	Tau,
	TauNeutrino,

	Gluon,
	Photon,
	WBoson,
	ZBoson,
	HiggsBoson
}
public class InitBlock : MonoBehaviour 
{
	public GameObject[] blockPrefabs;


}
