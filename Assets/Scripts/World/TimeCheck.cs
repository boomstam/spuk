﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeCheck
{
	int numFrames = 0;

	public bool UpdateTime(int framesPerTimeUnit)
	{
		bool cycleComplete = false;

		numFrames++;

		if (numFrames % framesPerTimeUnit == 0) {

			cycleComplete = true;
		}
		return cycleComplete;
	}
}
