﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Linq;

public class MapManager : MonoBehaviour 
{
	public GameObject[] tilePrefabs;

	public GameObject segmentPrefab;

	public GameObject[][] segment;

	public List<GameObject[][]> map = new List<GameObject[][]>();

	public Vector2 segmentSize;

	public Vector3 startPos;

	public int tileCount = 0;
	public int segmentCount = 0;

	void Start () 
	{
		InitMap ();
	}
	IEnumerator InstantiateSegment(Vector3 pos, int[][] typeArray)
	{
		segment = new GameObject[(int)(segmentSize.x * segmentSize.y)][];

		GameObject segmentHolder = GameObject.Instantiate(segmentPrefab);
		segmentHolder.transform.parent = gameObject.transform;
		segmentHolder.name = "segment_" + segmentCount;
		segmentCount++;

		for (int x = 0; x < segmentSize.x; x++) {

			GameObject[] yTiles = new GameObject[(int)segmentSize.y];

			for (int y = 0; y < segmentSize.y; y++) {

				int type = typeArray [x] [y];

				if (type >= 0){

					GameObject tile = GameObject.Instantiate (tilePrefabs [type]);

					tile.transform.position = pos + new Vector3 (x - segmentSize.x / 2, y - segmentSize.y / 2, 0);
					tile.transform.parent = segmentHolder.transform;
					tile.name = "tile_" + tileCount.ToString () + "_x_" + tile.transform.position.x + "_y_" + tile.transform.position.y;

					yTiles [y]= tile;
					tileCount++;
				}
				segment [x] = yTiles;
			}
			if (x % 16 == 0) {

				yield return 0;
			}
		}
		map.Add(segment);
	}
	void InitMap()
	{
		int[][] emptyArray = new CreateSegment ().GetEmptyTypeArray (segmentSize);
		int[][] filledArray = new CreateSegment ().GetFilledTypeArray (segmentSize);

		//StartCoroutine (InstantiateSegment(startPos, filledArray));
		//StartCoroutine (InstantiateSegment(startPos + new Vector3(segmentSize.x, 0, 0), filledArray));
	}
}
public class CreateSegment
{
	public int[][] GetEmptyTypeArray(Vector2 segmentSize)
	{
		int[][] emptyArray = new int[(int)segmentSize.x][];

		for (int x = 0; x < segmentSize.x; x++) {

			int[] yVals = new int[(int)segmentSize.y];

			for (int y = 0; y < segmentSize.y; y++) {

				yVals [y] = -1; 
			}
			emptyArray [x] = yVals;
		}
		return emptyArray;
	}
	public int[][] GetFilledTypeArray(Vector2 segmentSize)
	{
		int[][] filledArray = new int[(int)segmentSize.x][];

		for (int x = 0; x < segmentSize.x; x++) {

			int[] yVals = new int[(int)segmentSize.y];

			for (int y = 0; y < segmentSize.y; y++) {

				yVals [y] = 0; 
			}
			filledArray [x] = yVals;
		}
		return filledArray;
	}
}