﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
	None,
	Left,
	Right,
	Up,
	Down
}
public static class DirectionManager // NOT IN USE
{
	public static Vector3 Dir2Vect(Direction direction)
	{
		Vector3 dirVect = new Vector3 (0, 0, 0);
		
		switch (direction) {

		case Direction.None:
			dirVect = new Vector3 (0, 0, 0);
			break;
		case Direction.Left:
			dirVect = new Vector3 (-1, 0, 0);
			break;
		case Direction.Right:
			dirVect = new Vector3 (1, 0, 0);
			break;
		case Direction.Up:
			dirVect = new Vector3 (0, 1, 0);
			break;
		case Direction.Down:
			dirVect = new Vector3 (0, -1, 0);
			break;
		}
	return dirVect;
	}
}
