﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPlayer : MonoBehaviour 
{
	public GameObject playerPrefab;
	public Vector3 startPos;

	void Start () 
	{	
		GameObject player = GameObject.Instantiate (playerPrefab);
		player.transform.position = startPos;	
		player.transform.SetParent(gameObject.transform);
		player.name = "Player";

		Camera.main.GetComponent<UnityStandardAssets._2D.Camera2DFollow> ().target = player.GetComponentInChildren<Transform>();
	}
}  
