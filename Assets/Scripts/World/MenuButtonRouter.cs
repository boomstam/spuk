﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonRouter : MonoBehaviour 
{
	public GameObject rebindCanvas;

	public bool showingHotkeys = false;

	public void HotkeysButton()
	{
		if (showingHotkeys) {

			CanvasHandler.HideCanvas (rebindCanvas);
			CanvasHandler.ShowCanvas (gameObject);
			showingHotkeys = false;

		} else {

			CanvasHandler.ShowCanvas (rebindCanvas);
			CanvasHandler.HideCanvas (gameObject);
			showingHotkeys = true;
		}
	}
	public void QuitButton()
	{
		rebindCanvas.GetComponent<UI_Manager> ().HideMenu ();
	}
}
