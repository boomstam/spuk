﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRate : MonoBehaviour 
{
	public bool debugFrameRate = false;
	public bool debugFixedFrameRate = false;

	void Update () 
	{
		if(debugFrameRate)
			Debug.Log("frameRate" + 1.0f / Time.deltaTime);	
	}
	void FixedUpdate () 
	{
		if(debugFixedFrameRate)
			Debug.Log("fixedFrameRate" + 1.0f / Time.fixedDeltaTime);	
	}
}
