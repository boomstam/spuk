﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[AddComponentMenu("Hard Shell Studios/Examples/UI Manager")]
public class UI_Manager : MonoBehaviour
{
    public GameObject menu;

	bool showing = false;

	void Start()
	{
		/*hardInput.MouseLock(true);
		hardInput.MouseVisible(false);*/ // OUTCOMMENTED DURING TESTING
	}

	void Update ()
    {
        if (hardInput.GetKeyDown("Menu"))
        {
			if (showing || menu.GetComponent<MenuButtonRouter>().showingHotkeys)
            {
				HideMenu ();
            }
            else
            {
				ShowMenu ();
			}
        }
    }
	public void ShowMenu()
	{
		CanvasHandler.ShowCanvas (menu);

		hardInput.MouseLock(false);
		hardInput.MouseVisible(true);

		showing = true;
	}
	public void HideMenu()
	{
		CanvasHandler.HideCanvas (menu);
		CanvasHandler.HideCanvas (gameObject);
		menu.GetComponent<MenuButtonRouter> ().showingHotkeys = false;

		hardInput.MouseLock(true);
		hardInput.MouseVisible(false);

		showing = false;
	}
}
